provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "master" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  key_name      = "donkey"
  
  vpc_security_group_ids = ["sg-05e11433d93ba47a8"]

  tags = {
    Name = "puppet-master"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:\\Users\\Vsmart\\Downloads\\donkey.pem")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo su",
      "sudo run_puppet.sh" 
    ]
  }
}
