provider "aws" {
  region = "us-east-1"
}

data "aws_instances" "master" {
  instance_tags = {
    Name = "puppet-master"
  }
}

resource "aws_instance" "client" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  key_name      = "donkey"
  
  vpc_security_group_ids = ["sg-05e11433d93ba47a8"]

  tags = {
    Name = "puppet-client"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:\\Users\\Vsmart\\Downloads\\donkey.pem")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo su",
      "sudo nano /etc/hosts",
      "${data.aws_instances.master.instances[0].private_ip} puppet",
      "wget https://apt.puppetlabs.com/puppet8-release-jammy.deb",
      "sudo dpkg -i puppet8-release-jammy.deb",
      "sudo apt-get update",
      "sudo apt-get install puppet-agent -y",
      "sudo systemctl enable puppet",
      "sudo systemctl restart puppet",
      "sudo systemctl status puppet"
    ]
  }
}

resource "null_resource" "master_additional_steps" {
  count = var.master_additional_steps ? 1 : 0

  triggers = {
    run_after = aws_instance.master.id
  }

  provisioner "remote-exec" {
    inline = [
      "sudo su",
      "sudo /opt/puppetlabs/bin/puppetserver ca list --all",
      "sudo /opt/puppetlabs/bin/puppetserver ca sign --all",
      "/opt/puppetlabs/bin/puppet module install puppetlabs-apache --version 12.0.2",
      "sudo sh -c 'echo \"node 'hostname of client' {\" >> /etc/puppetlabs/code/environments/production/manifests/site.pp'",
      "sudo sh -c 'echo \"  include apache\" >> /etc/puppetlabs/code/environments/production/manifests/site.pp'",
      "sudo sh -c 'echo \"}\" >> /etc/puppetlabs/code/environments/production/manifests/site.pp'"
    ]
  }
}

resource "null_resource" "client_additional_steps" {
  count = var.client_additional_steps ? 1 : 0

  triggers = {
    run_after = aws_instance.client.id
  }

  provisioner "remote-exec" {
    inline = [
      "sudo su",
      "sudo sed -i 's/.*server *=.*/server = $(hostname -f)/' /etc/puppetlabs/puppet/puppet.conf",
      "sudo systemctl restart puppet",
      "/opt/puppetlabs/puppet/bin/puppet agent -t --server $(hostname -f) --debug"
    ]
  }
}
